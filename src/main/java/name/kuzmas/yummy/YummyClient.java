package name.kuzmas.yummy;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.minecraft.client.render.RenderLayer;

public class YummyClient implements ClientModInitializer
{
	@Override public void onInitializeClient()
	{
		BlockRenderLayerMap.INSTANCE.putBlocks(RenderLayer.getCutout(),
		    // clang-format off
		    Yummy.RICE_PLANT,
		    Yummy.TOMATO_PLANT
		    // clang-format on
		);
	}
}