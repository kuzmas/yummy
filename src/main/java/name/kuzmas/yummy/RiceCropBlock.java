package name.kuzmas.yummy;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.CropBlock;
import net.minecraft.block.ShapeContext;
import net.minecraft.item.ItemConvertible;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;

public class RiceCropBlock extends CropBlock
{
	private static final VoxelShape[] AGE_TO_SHAPE = new VoxelShape[] {
	    // clang-format off
			Block.createCuboidShape(0.0d, 0.0d, 0.0d, 16.0d,  4.0d, 16.0d),
	        Block.createCuboidShape(0.0d, 0.0d, 0.0d, 16.0d,  7.0d, 16.0d),
	        Block.createCuboidShape(0.0d, 0.0d, 0.0d, 16.0d, 10.0d, 16.0d),
	        Block.createCuboidShape(0.0d, 0.0d, 0.0d, 16.0d, 13.0d, 16.0d),
	        Block.createCuboidShape(0.0d, 0.0d, 0.0d, 16.0d, 16.0d, 16.0d),
	        Block.createCuboidShape(0.0d, 0.0d, 0.0d, 16.0d, 16.0d, 16.0d),
	        Block.createCuboidShape(0.0d, 0.0d, 0.0d, 16.0d, 16.0d, 16.0d),
	        Block.createCuboidShape(0.0d, 0.0d, 0.0d, 16.0d, 16.0d, 16.0d)
	    // clang-format on
	};

	public RiceCropBlock(AbstractBlock.Settings settings)
	{
		super(settings);
	}

	@Environment(EnvType.CLIENT) public ItemConvertible getSeedsItem()
	{
		return Yummy.RICE;
	}

	public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context)
	{
		return AGE_TO_SHAPE[(Integer)state.get(this.getAgeProperty())];
	}
}
