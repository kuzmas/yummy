package name.kuzmas.yummy;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ContainedItem extends Item
{
	private final Item container;

	public ContainedItem(Settings settings, Item container)
	{
		super(settings);
		this.container = container;
	}

	@Override public ItemStack finishUsing(ItemStack stack, World world, LivingEntity user)
	{
		PlayerEntity playerEntity = user instanceof PlayerEntity ? (PlayerEntity)user : null;
		if (playerEntity != null)
		{
			if (!playerEntity.getAbilities().creativeMode)
			{
				if (isFood())
				{
					user.eatFood(world, stack);
				}

				stack.decrement(1);
			}
		}

		return stack.isEmpty() ? new ItemStack(this.container) : stack;
	}
}
