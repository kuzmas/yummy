package name.kuzmas.yummy;

import com.google.common.collect.Lists;
import java.util.List;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.loot.v1.FabricLootPoolBuilder;
import net.fabricmc.fabric.api.loot.v1.FabricLootSupplierBuilder;
import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.ComposterBlock;
import net.minecraft.block.CropBlock;
import net.minecraft.block.Material;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.item.AliasedBlockItem;
import net.minecraft.item.FoodComponent;
import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Items;
import net.minecraft.loot.condition.RandomChanceLootCondition;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.provider.number.ConstantLootNumberProvider;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Yummy implements ModInitializer
{
	// This logger is used to write text to the console and the log file.
	// It is considered best practice to use your mod id as the logger's name.
	// That way, it's clear which mod wrote info, warnings, and errors.
	public static final Logger LOGGER = LogManager.getLogger("yummy");

	private static final List<LootTableInsert> SEED_LOOTS = Lists.newArrayList();

	public static final CropBlock RICE_PLANT = new RiceCropBlock(AbstractBlock.Settings.of(Material.PLANT)
	                                                                 .nonOpaque()
	                                                                 .noCollision()
	                                                                 .ticksRandomly()
	                                                                 .breakInstantly()
	                                                                 .sounds(BlockSoundGroup.CROP));

	public static final CropBlock TOMATO_PLANT = new TomatoCropBlock(AbstractBlock.Settings.of(Material.PLANT)
	                                                                     .nonOpaque()
	                                                                     .noCollision()
	                                                                     .ticksRandomly()
	                                                                     .breakInstantly()
	                                                                     .sounds(BlockSoundGroup.CROP));

	// clang-format off
	public static final Item TOMATO_SEEDS = new AliasedBlockItem(TOMATO_PLANT, new FabricItemSettings().group(ItemGroup.MISC));
	public static final Item RICE         = new AliasedBlockItem(RICE_PLANT,   new FabricItemSettings().group(ItemGroup.MISC));
	// clang-format on

	public static final Item APPLE_JUICE = new DrinkableItem(
	    new FabricItemSettings()
	        .group(ItemGroup.MISC)
	        .recipeRemainder(Items.GLASS_BOTTLE)
	        .maxCount(1)
	        .food(new FoodComponent.Builder()
	                  .alwaysEdible()
	                  .hunger(0)
	                  .saturationModifier(1.0f)
	                  // clang-format off
	                  .statusEffect(new StatusEffectInstance(StatusEffects.REGENERATION, (int)(3.0f /* sec */ * 20), 0), 1f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.GLOWING,      (int)(7.0f /* sec */ * 20), 0), 1f)
	                  // clang-format on
	                  .build()));

	public static final Item CARROT_JUICE = new DrinkableItem(
	    new FabricItemSettings()
	        .group(ItemGroup.MISC)
	        .recipeRemainder(Items.GLASS_BOTTLE)
	        .maxCount(1)
	        .food(new FoodComponent.Builder()
	                  .alwaysEdible()
	                  .hunger(0)
	                  .saturationModifier(1.0f)
	                  // clang-format off
	                  .statusEffect(new StatusEffectInstance(StatusEffects.NIGHT_VISION, (int)(3.0f /* sec */ * 20), 0), 1f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.GLOWING,      (int)(7.0f /* sec */ * 20), 0), 1f)
	                  // clang-format on
	                  .build()));

	public static final Item BERRY_JUICE = new DrinkableItem(
	    new FabricItemSettings()
	        .group(ItemGroup.MISC)
	        .recipeRemainder(Items.GLASS_BOTTLE)
	        .maxCount(1)
	        .food(new FoodComponent.Builder()
	                  .alwaysEdible()
	                  .hunger(0)
	                  .saturationModifier(1.0f)
	                  // clang-format off
	                  .statusEffect(new StatusEffectInstance(StatusEffects.SLOW_FALLING, (int)(3.0f /* sec */ * 20), 0), 1f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.GLOWING,      (int)(7.0f /* sec */ * 20), 0), 1f)
	                  // clang-format on
	                  .build()));

	public static final Item HOT_CHOCOLATE = new MilkItem(
	    new FabricItemSettings()
	        .group(ItemGroup.MISC)
	        .recipeRemainder(Items.GLASS_BOTTLE)
	        .maxCount(1)
	        .food(new FoodComponent.Builder()
	                  .alwaysEdible()
	                  .hunger(0)
	                  .saturationModifier(1.0f)
	                  // clang-format off
	                  .statusEffect(new StatusEffectInstance(StatusEffects.JUMP_BOOST, (int)(5.0f /* sec */ * 20), 0), 1f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.SPEED,      (int)(3.0f /* sec */ * 20), 0), 1f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.GLOWING,    (int)(7.0f /* sec */ * 20), 0), 1f)
	                  // clang-format on
	                  .build()));

	public static final Item APPLE_CIDER = new AlcoholItem(
	    new FabricItemSettings()
	        .group(ItemGroup.MISC)
	        .recipeRemainder(Items.GLASS_BOTTLE)
	        .maxCount(1)
	        .food(new FoodComponent.Builder()
	                  .alwaysEdible()
	                  .hunger(0)
	                  .saturationModifier(1.0f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.REGENERATION, (int)(4.0f /* sec */ * 20), 0), 1f)
	                  .build()),
	    1.0f, false);

	public static final Item BERRY_CIDER = new AlcoholItem(
	    new FabricItemSettings()
	        .group(ItemGroup.MISC)
	        .recipeRemainder(Items.GLASS_BOTTLE)
	        .maxCount(1)
	        .food(new FoodComponent.Builder()
	                  .alwaysEdible()
	                  .hunger(0)
	                  .saturationModifier(1.0f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.SLOW_FALLING, (int)(4.0f /* sec */ * 20), 0), 1f)
	                  .build()),
	    1.0f, false);

	public static final Item MEAD
	    = new AlcoholItem(new FabricItemSettings()
	                          .group(ItemGroup.MISC)
	                          .recipeRemainder(Items.GLASS_BOTTLE)
	                          .maxCount(1)
	                          .food(new FoodComponent.Builder().alwaysEdible().hunger(0).saturationModifier(1.0f).build()),
	        1.5f, false);

	public static final Item WHISKEY
	    = new AlcoholItem(new FabricItemSettings()
	                          .group(ItemGroup.MISC)
	                          .recipeRemainder(Items.GLASS_BOTTLE)
	                          .maxCount(1)
	                          .food(new FoodComponent.Builder().alwaysEdible().hunger(0).saturationModifier(1.0f).build()),
	        2.0f, true);

	// clang-format off
	public static final Item KETCHUP                   = miscItemInBottle();

	public static final Item RAW_CHIPS                 = miscItem();
	public static final Item RAW_POTATO_WITH_MUSHROOMS = miscItem();
	public static final Item RAW_FISH_N_CHIPS          = miscItem();
	public static final Item RAW_PIZZA                 = miscItem();
	public static final Item RAW_FRENCH_TOAST          = miscItem();
	public static final Item RAW_BACON_AND_EGG_PIE     = miscItem();
	public static final Item RAW_LASAGNE               = miscItem();
	public static final Item RAW_MEAT_PIE              = miscItem();
	public static final Item RAW_QUICHE_LORRAINE       = miscItem();

	public static final Item BARREL_OF_APPLE_CIDER     = miscItemInBarrel();
	public static final Item BARREL_OF_BERRY_CIDER     = miscItemInBarrel();
	public static final Item BARREL_OF_MEAD            = miscItemInBarrel();
	public static final Item BARREL_OF_WHISKEY         = miscItemInBarrel();
	// clang-format on

	// raw, not really edible food (poor)
	// clang-format off
	public static final Item RAW_POTATO_DUMPLINGS  = foodItem(1, 0.1f);
	public static final Item RAW_RABBIT_DUMPLINGS  = foodItem(1, 0.1f);
	public static final Item RAW_CHICKEN_DUMPLINGS = foodItem(1, 0.1f);
	public static final Item RAW_MUTTON_DUMPLINGS  = foodItem(1, 0.1f);
	public static final Item RAW_BEEF_DUMPLINGS    = foodItem(1, 0.1f);
	public static final Item RAW_PORK_DUMPLINGS    = foodItem(1, 0.1f);
	// clang-format on

	// raw food (low)
	// clang-format off
	public static final Item TOMATO       = foodItem(1, 0.3f);

	public static final Item RAW_SQUID    = foodItem(2, 0.3f);

	public static final Item RAW_GYROS    = foodItem(2, 0.3f);
	public static final Item RAW_KEBAB    = foodItem(2, 0.3f);
	public static final Item RAW_SHASHLIK = foodItem(2, 0.3f);
	public static final Item RAW_SOUVLAKI = foodItem(2, 0.3f);
	// clang-format on

	// sugar-based food
	public static final Item EGGNOG = new DrinkableItem(
	    new FabricItemSettings()
	        .group(ItemGroup.MISC)
	        .recipeRemainder(Items.GLASS_BOTTLE)
	        .maxCount(1)
	        .food(new FoodComponent.Builder()
	                  .hunger(1)
	                  .saturationModifier(0.2f)
	                  // clang-format off
	                  .statusEffect(new StatusEffectInstance(StatusEffects.LUCK,    (int)(3.0f /* sec */ * 20), 0), 1f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.GLOWING, (int)(7.0f /* sec */ * 20), 0), 1f)
	                  // clang-format on
	                  .build()));

	public static final Item CARAMEL = new Item(
	    new FabricItemSettings()
	        .group(ItemGroup.FOOD)
	        .food(new FoodComponent.Builder()
	                  .alwaysEdible()
	                  .hunger(1)
	                  .saturationModifier(0.2f)
	                  // clang-format off
	                  .statusEffect(new StatusEffectInstance(StatusEffects.SPEED,      (int)(7.0f /* sec */ * 20), 0), 1f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.JUMP_BOOST, (int)(3.5f /* sec */ * 20), 0), 1f)
	                  // clang-format on
	                  .build()));

	public static final Item CHOCOLATE = new Item(
	    new FabricItemSettings()
	        .group(ItemGroup.FOOD)
	        .food(new FoodComponent.Builder()
	                  .alwaysEdible()
	                  .hunger(1)
	                  .saturationModifier(0.2f)
	                  // clang-format off
	                  .statusEffect(new StatusEffectInstance(StatusEffects.JUMP_BOOST, (int)(7.0f /* sec */ * 20), 0), 1f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.SPEED,      (int)(3.5f /* sec */ * 20), 0), 1f)
	                  // clang-format on
	                  .build()));

	public static final Item ICECREAM = new Item(
	    new FabricItemSettings()
	        .group(ItemGroup.FOOD)
	        .maxCount(8)
	        .food(new FoodComponent.Builder()
	                  .alwaysEdible()
	                  .hunger(1)
	                  .saturationModifier(0.2f)
	                  // clang-format off
	                  .statusEffect(new StatusEffectInstance(StatusEffects.SLOWNESS, (int)(2.0f /* sec */ * 20), 0), 1f)
	                  // clang-format on
	                  .build()));

	public static final Item CARAMEL_ICECREAM = new Item(
	    new FabricItemSettings()
	        .group(ItemGroup.FOOD)
	        .maxCount(8)
	        .food(new FoodComponent.Builder()
	                  .alwaysEdible()
	                  .hunger(2)
	                  .saturationModifier(0.2f)
	                  // clang-format off
	                  .statusEffect(new StatusEffectInstance(StatusEffects.SLOWNESS,   (int)(2.0f /* sec */ * 20), 0), 1f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.SPEED,      (int)(8.0f /* sec */ * 20), 0), 1f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.JUMP_BOOST, (int)(4.5f /* sec */ * 20), 0), 1f)
	                  // clang-format on
	                  .build()));

	public static final Item CHOCOLATE_ICECREAM = new Item(
	    new FabricItemSettings()
	        .group(ItemGroup.FOOD)
	        .maxCount(8)
	        .food(new FoodComponent.Builder()
	                  .alwaysEdible()
	                  .hunger(2)
	                  .saturationModifier(0.2f)
	                  // clang-format off
	                  .statusEffect(new StatusEffectInstance(StatusEffects.SLOWNESS,   (int)(2.0f /* sec */ * 20), 0), 1f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.JUMP_BOOST, (int)(8.0f /* sec */ * 20), 0), 1f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.SPEED,      (int)(4.5f /* sec */ * 20), 0), 1f)
	                  // clang-format on
	                  .build()));

	public static final Item BERRY_ICECREAM = new Item(
	    new FabricItemSettings()
	        .group(ItemGroup.FOOD)
	        .maxCount(8)
	        .food(new FoodComponent.Builder()
	                  .alwaysEdible()
	                  .hunger(2)
	                  .saturationModifier(0.2f)
	                  // clang-format off
	                  .statusEffect(new StatusEffectInstance(StatusEffects.SLOWNESS,     (int)(2.0f /* sec */ * 20), 0), 1f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.SLOW_FALLING, (int)(8.0f /* sec */ * 20), 0), 1f)
	                  .statusEffect(new StatusEffectInstance(StatusEffects.JUMP_BOOST,   (int)(4.5f /* sec */ * 20), 0), 1f)
	                  // clang-format on
	                  .build()));

	// clang-format off

	// cooked food (normal)
	public static final Item PITA                        = foodItem      ( 3, 0.6f);

	public static final Item CHEESE_WEDGE                = foodItem      ( 1, 0.6f);

	public static final Item BOWL_OF_RICE                = foodItemInBowl( 4, 0.6f);
	public static final Item RISOTTO                     = foodItemInBowl( 6, 0.6f);

	public static final Item APPLE_PIE                   = foodItem      ( 7, 0.6f);
	public static final Item BERRY_PIE                   = foodItem      ( 5, 0.6f);

	public static final Item PUMPKIN_MUFFIN              = foodItem      ( 1, 0.6f);
	public static final Item BERRY_MUFFIN                = foodItem      ( 2, 0.6f);

	public static final Item PANCAKES                    = foodItem      ( 3, 0.6f);

	public static final Item CHIPS                       = foodItem      ( 5, 0.6f);
	public static final Item CHIPS_WITH_KETCHUP          = foodItem	     ( 7, 0.6f);

	public static final Item FRUIT_SALAD                 = foodItemInBowl( 3, 0.6f);
	public static final Item VEGE_SALAD                  = foodItemInBowl( 3, 0.6f);

	public static final Item POTATO_DUMPLINGS            = foodItem      ( 3, 0.6f);

	public static final Item FRIED_EGG                   = foodItem      ( 2, 0.6f);
	public static final Item CALAMARI                    = foodItem      ( 5, 0.6f);

	public static final Item FISH_STUFFED_EGGS           = foodItem      ( 4, 0.6f);
	public static final Item MUSHROOM_STUFFED_EGGS       = foodItem      ( 3, 0.6f);

	public static final Item GYROS                       = foodItem      ( 3, 0.6f);
	public static final Item SHASHLIK                    = foodItem      ( 3, 0.6f);
	public static final Item KEBAB                       = foodItem      ( 4, 0.6f);
	public static final Item SOUVLAKI                    = foodItem      ( 4, 0.6f);

	public static final Item HONEY_GLAZED_ROAST_CHICKEN  = foodItem      ( 8, 0.6f);

	// cooked balanced food	(good)
	public static final Item BEEF_SANDWICH               = foodItem      ( 4, 0.8f);
	public static final Item CHEESE_SANDWICH             = foodItem      ( 2, 0.8f);
	public static final Item CHICKEN_SANDWICH            = foodItem      ( 3, 0.8f);
	public static final Item COD_SANDWICH                = foodItem      ( 3, 0.8f);
	public static final Item MUTTON_SANDWICH             = foodItem      ( 3, 0.8f);
	public static final Item SALMON_SANDWICH             = foodItem      ( 3, 0.8f);
	public static final Item PORK_SANDWICH               = foodItem      ( 4, 0.8f);
	public static final Item FRENCH_TOAST                = foodItem      ( 2, 0.8f);

	public static final Item CHEESEBURGER                = foodItem      ( 6, 0.8f);
	public static final Item HAMBURGER                   = foodItem      ( 5, 0.8f);

	public static final Item BEEF_TACO                   = foodItem      ( 8, 0.8f);
	public static final Item CHICKEN_TACO                = foodItem      ( 6, 0.8f);
	public static final Item FISH_TACO                   = foodItem      ( 5, 0.8f);
	public static final Item PORK_TACO                   = foodItem      ( 7, 0.8f);
	public static final Item SQUID_TACO                  = foodItem      ( 5, 0.8f);

	public static final Item CHICKEN_DUMPLINGS           = foodItem      ( 3, 0.8f);
	public static final Item MUTTON_DUMPLINGS            = foodItem      ( 3, 0.8f);
	public static final Item RABBIT_DUMPLINGS            = foodItem      ( 3, 0.8f);
	public static final Item BEEF_DUMPLINGS              = foodItem      ( 4, 0.8f);
	public static final Item PORK_DUMPLINGS              = foodItem      ( 4, 0.8f);

	public static final Item COD_SUSHI                   = foodItem      ( 5, 0.8f);
	public static final Item SALMON_SUSHI                = foodItem      ( 5, 0.8f);
	public static final Item SQUID_SUSHI                 = foodItem      ( 5, 0.8f);

	public static final Item FISH_N_CHIPS                = foodItem      (12, 0.8f);
	public static final Item FRIED_POTATO_WITH_MUSHROOMS = foodItem      ( 7, 0.8f);

	public static final Item BACON_AND_EGG_PIE           = foodItem      ( 6, 0.8f);
	public static final Item MEAT_PIE                    = foodItem      ( 8, 0.8f);

	public static final Item POTATO_SALAD                = foodItemInBowl( 7, 0.8f);
	public static final Item SEAFOOD_SALAD               = foodItemInBowl( 7, 0.8f);

	public static final Item PIZZA                       = foodItem      (16, 0.8f);
	public static final Item PIZZA_SLICE                 = foodItem      ( 2, 0.8f);

	public static final Item LASAGNE                     = foodItem      ( 8, 0.8f);
	public static final Item PILAF                       = foodItemInBowl( 8, 0.8f);
	public static final Item POT_AU_FEU                  = foodItemInBowl( 6, 0.8f);
	public static final Item QUICHE_LORRAINE             = foodItem      ( 8, 0.8f);
	public static final Item SPAGHETTI_WITH_MEATBALLS    = foodItemInBowl( 7, 0.8f);
	public static final Item STIR_FRY                    = foodItemInBowl( 7, 0.8f);

	public static final Item TOMATO_SOUP                 = foodItemInBowl( 7, 0.8f);
	public static final Item FISH_SOUP                   = foodItemInBowl( 7, 0.8f);
	public static final Item BORSHTCH                    = foodItemInBowl( 6, 0.8f);

	// clang-format on

	@Override public void onInitialize()
	{
		// This code runs as soon as Minecraft is in a mod-load-ready state.
		// However, some things (like resources) may still be uninitialized.
		// Proceed with mild caution.

		// LOGGER.info("Hello Yummy!");

		// clang-format off
		registerBlock("rice_plant",   Yummy.RICE_PLANT);
		registerBlock("tomato_plant", Yummy.TOMATO_PLANT);

		registerItem("tomato_seeds",                Yummy.TOMATO_SEEDS);

		registerItem("barrel_of_apple_cider",       Yummy.BARREL_OF_APPLE_CIDER);
		registerItem("apple_cider",                 Yummy.APPLE_CIDER);
		registerItem("barrel_of_berry_cider",       Yummy.BARREL_OF_BERRY_CIDER);
		registerItem("berry_cider",                 Yummy.BERRY_CIDER);
		registerItem("barrel_of_mead",              Yummy.BARREL_OF_MEAD);
		registerItem("mead",                        Yummy.MEAD);
		registerItem("barrel_of_whiskey",           Yummy.BARREL_OF_WHISKEY);
		registerItem("whiskey",                     Yummy.WHISKEY);

		registerItem("tomato",                      Yummy.TOMATO);
		registerItem("rice",                        Yummy.RICE);
		registerItem("raw_squid",                   Yummy.RAW_SQUID);

		registerItem("raw_potato_dumplings",        Yummy.RAW_POTATO_DUMPLINGS);
		registerItem("potato_dumplings",            Yummy.POTATO_DUMPLINGS);
		registerItem("raw_rabbit_dumplings",        Yummy.RAW_RABBIT_DUMPLINGS);
		registerItem("rabbit_dumplings",            Yummy.RABBIT_DUMPLINGS);
		registerItem("raw_chicken_dumplings",       Yummy.RAW_CHICKEN_DUMPLINGS);
		registerItem("chicken_dumplings",           Yummy.CHICKEN_DUMPLINGS);
		registerItem("raw_mutton_dumplings",        Yummy.RAW_MUTTON_DUMPLINGS);
		registerItem("mutton_dumplings",            Yummy.MUTTON_DUMPLINGS);
		registerItem("raw_pork_dumplings",          Yummy.RAW_PORK_DUMPLINGS);
		registerItem("pork_dumplings",              Yummy.PORK_DUMPLINGS);
		registerItem("raw_beef_dumplings",          Yummy.RAW_BEEF_DUMPLINGS);
		registerItem("beef_dumplings",              Yummy.BEEF_DUMPLINGS);

		registerItem("raw_gyros",                   Yummy.RAW_GYROS);
		registerItem("gyros",                       Yummy.GYROS);
		registerItem("raw_shashlik",                Yummy.RAW_SHASHLIK);
		registerItem("shashlik",                    Yummy.SHASHLIK);
		registerItem("raw_souvlaki",                Yummy.RAW_SOUVLAKI);
		registerItem("souvlaki",                    Yummy.SOUVLAKI);
		registerItem("raw_kebab",                   Yummy.RAW_KEBAB);
		registerItem("kebab",                       Yummy.KEBAB);

		registerItem("raw_french_toast",            Yummy.RAW_FRENCH_TOAST);

		registerItem("raw_chips",                   Yummy.RAW_CHIPS);
		registerItem("chips",                       Yummy.CHIPS);
		registerItem("chips_with_ketchup",          Yummy.CHIPS_WITH_KETCHUP);
		registerItem("raw_potato_with_mushrooms",   Yummy.RAW_POTATO_WITH_MUSHROOMS);
		registerItem("fried_potato_with_mushrooms", Yummy.FRIED_POTATO_WITH_MUSHROOMS);
		registerItem("raw_fish_n_chips",            Yummy.RAW_FISH_N_CHIPS);
		registerItem("fish_n_chips",                Yummy.FISH_N_CHIPS);

		registerItem("raw_pizza",                   Yummy.RAW_PIZZA);

		registerItem("calamari",                    Yummy.CALAMARI);
		registerItem("fried_egg",                   Yummy.FRIED_EGG);
		registerItem("bowl_of_rice",                Yummy.BOWL_OF_RICE);
		registerItem("pita",                        Yummy.PITA);
		registerItem("cheese_wedge",                Yummy.CHEESE_WEDGE);
		registerItem("ketchup",                     Yummy.KETCHUP);

		registerItem("french_toast",                Yummy.FRENCH_TOAST);
		registerItem("cheese_sandwich",             Yummy.CHEESE_SANDWICH);
		registerItem("cod_sandwich",                Yummy.COD_SANDWICH);
		registerItem("salmon_sandwich",             Yummy.SALMON_SANDWICH);
		registerItem("chicken_sandwich",            Yummy.CHICKEN_SANDWICH);
		registerItem("mutton_sandwich",             Yummy.MUTTON_SANDWICH);
		registerItem("pork_sandwich",               Yummy.PORK_SANDWICH);
		registerItem("beef_sandwich",               Yummy.BEEF_SANDWICH);

		registerItem("hamburger",                   Yummy.HAMBURGER);
		registerItem("cheeseburger",                Yummy.CHEESEBURGER);

		registerItem("raw_bacon_and_egg_pie",       Yummy.RAW_BACON_AND_EGG_PIE);
		registerItem("bacon_and_egg_pie",           Yummy.BACON_AND_EGG_PIE);
		registerItem("raw_lasagne",	                Yummy.RAW_LASAGNE);
		registerItem("lasagne",                     Yummy.LASAGNE);
		registerItem("raw_meat_pie",                Yummy.RAW_MEAT_PIE);
		registerItem("meat_pie",                    Yummy.MEAT_PIE);
		registerItem("raw_quiche_lorraine",	        Yummy.RAW_QUICHE_LORRAINE);
		registerItem("quiche_lorraine",             Yummy.QUICHE_LORRAINE);

		registerItem("squid_taco",                  Yummy.SQUID_TACO);
		registerItem("fish_taco",                   Yummy.FISH_TACO);
		registerItem("chicken_taco",                Yummy.CHICKEN_TACO);
		registerItem("pork_taco",                   Yummy.PORK_TACO);
		registerItem("beef_taco",                   Yummy.BEEF_TACO);

		registerItem("cod_sushi",                   Yummy.COD_SUSHI);
		registerItem("salmon_sushi",                Yummy.SALMON_SUSHI);
		registerItem("squid_sushi",                 Yummy.SQUID_SUSHI);

		registerItem("pizza",                       Yummy.PIZZA);
		registerItem("pizza_slice",                 Yummy.PIZZA_SLICE);

		registerItem("borshtch",                    Yummy.BORSHTCH);
		registerItem("fish_soup",                   Yummy.FISH_SOUP);
		registerItem("tomato_soup",                 Yummy.TOMATO_SOUP);

		registerItem("fish_stuffed_eggs",           Yummy.FISH_STUFFED_EGGS);
		registerItem("mushroom_stuffed_eggs",       Yummy.MUSHROOM_STUFFED_EGGS);

		registerItem("vege_salad",                  Yummy.VEGE_SALAD);
		registerItem("fruit_salad",                 Yummy.FRUIT_SALAD);
		registerItem("potato_salad",                Yummy.POTATO_SALAD);
		registerItem("seafood_salad",               Yummy.SEAFOOD_SALAD);

		registerItem("honey_glazed_roast_chicken",  Yummy.HONEY_GLAZED_ROAST_CHICKEN);
		registerItem("pot_au_feu",                  Yummy.POT_AU_FEU);
		registerItem("risotto",                     Yummy.RISOTTO);
		registerItem("pilaf",                       Yummy.PILAF);
		registerItem("spaghetti_with_meatballs",    Yummy.SPAGHETTI_WITH_MEATBALLS);
		registerItem("stir_fry",                    Yummy.STIR_FRY);

		registerItem("apple_juice",                 Yummy.APPLE_JUICE);
		registerItem("berry_juice",                 Yummy.BERRY_JUICE);
		registerItem("carrot_juice",                Yummy.CARROT_JUICE);
		registerItem("eggnog",                      Yummy.EGGNOG);
		registerItem("hot_chocolate",               Yummy.HOT_CHOCOLATE);

		registerItem("caramel",                     Yummy.CARAMEL);
		registerItem("chocolate",                   Yummy.CHOCOLATE);

		registerItem("apple_pie",                   Yummy.APPLE_PIE);
		registerItem("berry_pie",                   Yummy.BERRY_PIE);

		registerItem("berry_muffin",                Yummy.BERRY_MUFFIN);
		registerItem("pumpkin_muffin",              Yummy.PUMPKIN_MUFFIN);
		registerItem("pancakes",                    Yummy.PANCAKES);

		registerItem("icecream",                    Yummy.ICECREAM);
		registerItem("berry_icecream",              Yummy.BERRY_ICECREAM);
		registerItem("caramel_icecream",            Yummy.CARAMEL_ICECREAM);
		registerItem("chocolate_icecream",          Yummy.CHOCOLATE_ICECREAM);

		addSeedLoot("blocks/grass",      Items.POTATO);
		addSeedLoot("blocks/grass",      Items.CARROT);
		addSeedLoot("blocks/grass",      Items.BEETROOT_SEEDS);
		addSeedLoot("blocks/grass",      Yummy.TOMATO_SEEDS);
		addSeedLoot("blocks/sugar_cane", Yummy.RICE);
		// clang-format on

		// Perform Callback insertion
		LootTableLoadingCallback.EVENT.register(((resourceManager, lootManager, identifier, supplier, lootTableSetter) -> {
			SEED_LOOTS.forEach(i -> {
				if (ArrayUtils.contains(i.tables, identifier))
				{
					i.insert(supplier);
				}
			});
		}));

		registerCompostableItem(Yummy.RICE, 0.4f);
		registerCompostableItem(Yummy.TOMATO_SEEDS, 0.3f);
		registerCompostableItem(Yummy.TOMATO, 0.65f);

		registerCompostableItem(Items.POISONOUS_POTATO, 0.2f);
	}

	private static Item miscItemInBottle()
	{
		return new ContainedItem(
		    new FabricItemSettings().group(ItemGroup.MISC).recipeRemainder(Items.GLASS_BOTTLE).maxCount(16), Items.GLASS_BOTTLE);
	}

	private static Item miscItem()
	{
		return new Item(new FabricItemSettings().group(ItemGroup.MISC));
	}

	private static Item miscItemInBarrel()
	{
		return new Item(new FabricItemSettings().group(ItemGroup.MISC).recipeRemainder(Items.BARREL));
	}

	private static Item foodItem(int hunger, float saturation)
	{
		return new Item(new FabricItemSettings()
		                    .group(ItemGroup.FOOD)
		                    .food(new FoodComponent.Builder().hunger(hunger).saturationModifier(saturation).build()));
	}

	private static Item foodItemInBowl(int hunger, float saturation)
	{
		return new ContainedItem(new FabricItemSettings()
		                             .group(ItemGroup.FOOD)
		                             .recipeRemainder(Items.BOWL)
		                             .maxCount(1)
		                             .food(new FoodComponent.Builder().hunger(hunger).saturationModifier(saturation).build()),
		    Items.BOWL);
	}

	private static void registerBlock(String name, Block block)
	{
		Registry.register(Registry.BLOCK, new Identifier("yummy", name), block);
	}

	private static void registerItem(String name, Item item)
	{
		Registry.register(Registry.ITEM, new Identifier("yummy", name), item);
	}

	public static void addSeedLoot(String source, Item target)
	{
		SEED_LOOTS.add(new LootTableInsert(FabricLootPoolBuilder.builder()
		                                       .rolls(ConstantLootNumberProvider.create(1))
		                                       .with(ItemEntry.builder(target))
		                                       .withCondition(RandomChanceLootCondition.builder(0.05F).build()),
		    new Identifier("minecraft", source)));
	}

	public static class LootTableInsert
	{
		public final Identifier[] tables;
		public final FabricLootPoolBuilder lootPool;

		public LootTableInsert(FabricLootPoolBuilder lootPool, Identifier... tables)
		{
			this.tables = tables;
			this.lootPool = lootPool;
		}

		public void insert(FabricLootSupplierBuilder supplier)
		{
			supplier.pool(lootPool);
		}
	}

	public static void registerCompostableItem(ItemConvertible item, float chance)
	{
		if (item.asItem() != Items.AIR)
		{
			ComposterBlock.ITEM_TO_LEVEL_INCREASE_CHANCE.put(item.asItem(), chance);
		}
	}
}
