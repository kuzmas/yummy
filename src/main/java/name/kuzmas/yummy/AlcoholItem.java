package name.kuzmas.yummy;

import java.util.Timer;
import java.util.TimerTask;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class AlcoholItem extends DrinkableItem
{
	class EffectTimerTask extends TimerTask
	{
		private final PlayerEntity playerEntity;
		private final StatusEffect statusEffect;
		private final int duration;

		EffectTimerTask(PlayerEntity playerEntity, StatusEffect statusEffect, int duration)
		{
			this.playerEntity = playerEntity;
			this.statusEffect = statusEffect;
			this.duration = duration;
		}

		public void run()
		{
			this.playerEntity.addStatusEffect(new StatusEffectInstance(this.statusEffect, this.duration, 0));
		}
	}

	private final float effectLevel;

	private final boolean fireWater;

	public AlcoholItem(Settings settings, float effectLevel, boolean fireWater)
	{
		super(settings);
		this.effectLevel = effectLevel;
		this.fireWater = fireWater;
	}

	@Override public ItemStack finishUsing(ItemStack stack, World world, LivingEntity user)
	{
		PlayerEntity playerEntity = user instanceof PlayerEntity ? (PlayerEntity)user : null;
		if (playerEntity != null)
		{
			if (!playerEntity.getAbilities().creativeMode)
			{
				playerEntity.addStatusEffect(
				    new StatusEffectInstance(StatusEffects.LUCK, (int)(this.effectLevel * 5 /* sec */ * 20), 0));

				if (this.fireWater)
				{
					playerEntity.setOnFireFor((int)(this.effectLevel));
				}

				Timer timer1 = new Timer();
				timer1.schedule(
				    new EffectTimerTask(playerEntity, StatusEffects.HASTE, (int)(this.effectLevel * 5 /* sec */ * 20)),
				    (int)(this.effectLevel * 1 /* sec */ * 1000));

				Timer timer2 = new Timer();
				timer2.schedule(
				    new EffectTimerTask(playerEntity, StatusEffects.STRENGTH, (int)(this.effectLevel * 5 /* sec */ * 20)),
				    (int)(this.effectLevel * 2 /* sec */ * 1000));

				Timer timer3 = new Timer();
				timer3.schedule(
				    new EffectTimerTask(playerEntity, StatusEffects.RESISTANCE, (int)(this.effectLevel * 5 /* sec */ * 20)),
				    (int)(this.effectLevel * 3 /* sec */ * 1000));

				Timer timer4 = new Timer();
				timer4.schedule(
				    new EffectTimerTask(playerEntity, StatusEffects.ABSORPTION, (int)(this.effectLevel * 5 /* sec */ * 20)),
				    (int)(this.effectLevel * 4 /* sec */ * 1000));

				Timer timer5 = new Timer();
				timer5.schedule(
				    new EffectTimerTask(playerEntity, StatusEffects.SLOWNESS, (int)(this.effectLevel * 5 /* sec */ * 20)),
				    (int)(this.effectLevel * 5 /* sec */ * 1000));

				Timer timer6 = new Timer();
				timer6.schedule(
				    new EffectTimerTask(playerEntity, StatusEffects.MINING_FATIGUE, (int)(this.effectLevel * 5 /* sec */ * 20)),
				    (int)(this.effectLevel * 6 /* sec */ * 1000));

				Timer timer7 = new Timer();
				timer7.schedule(
				    new EffectTimerTask(playerEntity, StatusEffects.WEAKNESS, (int)(this.effectLevel * 5 /* sec */ * 20)),
				    (int)(this.effectLevel * 7 /* sec */ * 1000));

				Timer timer8 = new Timer();
				timer8.schedule(
				    new EffectTimerTask(playerEntity, StatusEffects.POISON, (int)(this.effectLevel * 5 /* sec */ * 20)),
				    (int)(this.effectLevel * 8 /* sec */ * 1000));

				Timer timer9 = new Timer();
				timer9.schedule(
				    new EffectTimerTask(playerEntity, StatusEffects.NAUSEA, (int)(this.effectLevel * 5 /* sec */ * 20)),
				    (int)(this.effectLevel * 9 /* sec */ * 1000));
			}
		}

		return super.finishUsing(stack, world, user);
	}
}
