# Yummy
A Minecraft mod which re-balances the hunger/saturation and adds more food

# Description
The main idea is that protein food is more saturating than carbohydrate food, cooked food is better than raw and nothing can beat a good balanced diet!

Also food made from other kinds of food should be no worse than its ingredients.
 
Keeping that in mind the food is divided into the categories based on their saturation ratio:

* 0.2 - not really food
* 0.2 - raw, not really edible vegetables
* 0.6 - raw edible vegetables 
* 0.8 - raw edible vegetables (advanced)
* 0.8 - raw protein food
* 0.8 - sugar-based food
* 1.0 - sugar-based food (advanced)
* 1.2 - carbohydrate food
* 1.4 - carbohydrate food (advanced)
* 1.4 - cooked vegetables
* 1.6 - cooked vegetables (advanced)
* 1.8 - cooked protein food
* 2.0 - cooked balanced food
* 2.4 - magic food

# Config
There are no config yet.

# Achievements
There are no new achievements yet (there are plans for them in the future).

Though existing ones are adjusted where needed. I mean the recipe for bread has changed and now requires yeast, which in its turn requires water in a bucket and therefore iron. Yes, it's now much more difficult to get "Bake Bread" achievement. To compensate for that, crafting a pita will also give you this "Bake Bread" achievement.

Oh, and the recipe for the cake is also adjusted, but the impact is less significant.

# Initial resource acquisition
In addition to wheat seeds, grass drops carrot, potato, beetroot seeds, tomato seeds, sunflower seeds and rice. So basically all seeds except melons and pumpkins. It also means that you no longer depend on villages to get potatoes, carrots and beetroots. (In the future it can probably by controlled by options in config, but, again, at the moment there are no config in this mod yet).

Rice also drops from sugar cane.

# Compatibility
Yummy is compatible with Biomes O'Plenty and partially with Ceramics.

# Legal stuff
Copyright 2017 Kuzma Shapran

Yummy is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Yummy is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with Yummy. If not, see <http://www.gnu.org/licenses/>.

You may use this mod in a modpack as long as you do not make money off of it (Adf.ly, exclusive access from donations, etc.), you give credit (Linking back to https://minecraft.curseforge.com/projects/yummy and/or https://github.com/kuzmas/yummy) and you follow all the terms listed in the license.
 
I would appreciate if you notify me prior to inclusion the mod in a modpack.
